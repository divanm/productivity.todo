﻿using Microsoft.AspNetCore.Mvc;

namespace Productivity.ToDo.UI.Controllers
{
    public class CompositionDemoController : Controller
    {
        public IActionResult Index()
        {

            // No code in these controllers - we use composition

            return View();
        }
    }
}