﻿using Microsoft.AspNetCore.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Productivity.ToDo.UI.Blazor.Client
{
    public class ApiClient
    {
        const string BaseUrl = "https://localhost:44334/api/v2.0/"; //TODO: Config

        private readonly HttpClient _httpClient;

        public ApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<ToDoResponse>> GetToDoListAsync()
        {
            var response = await _httpClient.GetJsonAsync<IEnumerable<ToDoResponse>>($"{BaseUrl}/ToDoItems");
            return response;
        }

        public async Task<HttpResponseMessage> MarkAsDoneAsync(Guid id)
        {
            var response = await _httpClient.PostAsync((string.Format("ToDoItems/Done/{0}/", BaseUrl, id)), null);
            return response;
        }

        //public async Task<HttpResponseMessage> CreatTaskAsync(string text)
        //{
           

        //}

       
    }
}
