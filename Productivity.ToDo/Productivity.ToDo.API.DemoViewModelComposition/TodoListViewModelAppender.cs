﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Productivity.ToDo.API.Ops;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Productivity.ToDo.API.DemoViewModelCompisition
{
    public class TodoListViewModelAppender : IViewModelAppender
    {
        // Matching is a bit weak in this demo.
        public bool Matches(RouteData routeData, string httpMethod) =>
            HttpMethods.IsGet(httpMethod)
                && string.Equals((string)routeData.Values["controller"], "CompositionDemo", StringComparison.OrdinalIgnoreCase)
                && !routeData.Values.ContainsKey("id");

        public async Task Append(dynamic viewModel, RouteData routeData, IQueryCollection query)
        {
            // Hardcoded to simplify the demo. In a production app, a config object could be injected.
            var url = $"https://localhost:44334/api/v2.0/ToDoItems";
            var response = await new HttpClient().GetAsync(url);


            dynamic[] todos = await response.Content.AsExpandoArrayAsync();

            var todosViewModelDictionary = todos;

            await viewModel.RaiseEventAsync(new ToDoItemsLoaded { TodoItems = todos });

            viewModel.ToDos = todos;
        }
    }
}