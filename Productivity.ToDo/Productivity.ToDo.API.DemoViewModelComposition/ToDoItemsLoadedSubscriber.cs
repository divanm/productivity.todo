﻿using AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Productivity.ToDo.API.Ops;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;


namespace Productivity.ToDo.API.DemoViewModelCompisition
{

    public class ToDoItemsLoadedSubscriber : ISubscribeToCompositionEvents
    {
        // Very simple matching for the purpose of the exercise
        public bool Matches(RouteData routeData, string httpMethod) =>
            HttpMethods.IsGet(httpMethod)
                && string.Equals((string)routeData.Values["controller"], "CompositionDemo", StringComparison.OrdinalIgnoreCase)
                && !routeData.Values.ContainsKey("id");

        public void Subscribe(IPublishCompositionEvents publisher)
        {
            publisher.Subscribe<ToDoItemsLoaded>(async (pageViewModel, todosLoaded, routeData, query) =>
            {
                var ids = todosLoaded.TodoItems.Select(t => t.Id).ToList();

                //Set some random priority that comes from some new awesome ML API the company built, it calculates and persists importance on another stack ....
                // Hardcoded to simplify the exercise.In a production app, a config object could be injected.
                var url = $"https://localhost:44334/api/v2.0/Importance";
                var response = await new HttpClient().PostAsJsonAsync<dynamic>(url, ids);

                dynamic[] ratings = await response.Content.AsExpandoArrayAsync();

                foreach (dynamic t in todosLoaded.TodoItems)
                {
                    t.Info = ratings.SingleOrDefault(r => r.Id == t.Id)?.Info;
                }
            });
        }
    }
}