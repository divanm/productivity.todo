﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Productivity.ToDo.Data;
using Productivity.ToDo.Shared;

namespace Productivity.ToDo.API.Controllers.V2
{
    [ApiVersion("2.0")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class ToDoItemsController : ControllerBase
    {
        private readonly ToDoContext _context;

        public ToDoItemsController(ToDoContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var todos = await _context.ToDoItems
            .OrderBy(t => t.Done)
            .ThenBy(t => t.Text).ToListAsync();

            var viewItems = todos.Select(
                t => new ToDoView
                { Created = t.Created, Done = t.Done, Id = t.Id, Text = t.Text });

            return Ok(viewItems);
        }

    }
}