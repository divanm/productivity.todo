﻿namespace Productivity.ToDo.API.Ops
{
    public interface ISubscribeToCompositionEvents : IRouteInterceptor
    {
        void Subscribe(IPublishCompositionEvents publisher);
    }
}