﻿using System;

namespace Productivity.ToDo.API.Ops
{
    public interface IPublishCompositionEvents
    {
        void Subscribe<TEvent>(EventHandler<TEvent> handler);
    }
}